source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "2.6.6"

gem "rails", "~> 6.0.3", ">= 6.0.3.2"

# Core
gem "bcrypt", "~> 3.1.7"
gem "bootsnap", ">= 1.4.2", require: false
gem "jbuilder", "~> 2.7"
gem "pg", ">= 0.18", "< 2.0"
gem "puma", "~> 4.1"
gem "sass-rails", ">= 6"
gem "turbolinks", "~> 5"
gem "uglifier", ">= 1.3.0"
gem "webpacker", "~> 4.0"

# Jbaker specific
gem "administrate"
gem "devise"
gem "name_of_person"
gem "paper_trail"
gem "pg_search"
gem "pundit"
gem "stimulus_reflex", "~> 3.2"
gem "strip_attributes"
gem "turbolinks_render"
gem "view_component"

group :development, :test do
  gem "awesome_print"
  gem "byebug"
  gem "brakeman"
  gem "factory_bot_rails"
  gem "faker"
  gem "pry-rails"
  gem "rspec-rails", "~> 4.0.1"
  gem "standard"
end

group :development do
  gem "annotate"
  gem "listen", "~> 3.2"
  gem "meta_request"
  gem "spring"
  gem "spring-commands-rspec"
  gem "spring-watcher-listen", "~> 2.0.0"
  gem "web-console", ">= 3.3.0"
end

group :test do
  gem "capybara", ">= 2.15"
  gem "selenium-webdriver"
  gem "simplecov"
  gem "webdrivers"
end
